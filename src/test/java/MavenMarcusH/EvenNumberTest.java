package MavenMarcusH;

import static org.junit.Assert.*;

import org.junit.Test;

public class EvenNumberTest {

	@Test
	public void testEvenNumber() {
		EvenNumber en = new EvenNumber();
		assertEquals("10 är ett jämt nummer", true, en.Number(10));
		
	}

}
